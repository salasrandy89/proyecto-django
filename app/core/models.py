from django.db import models
from django.utils import timezone
from datetime import datetime

# Create your models here.

class Cargo(models.Model):
    nombre_cargo = models.CharField(max_length=150, unique=True, verbose_name='Nombre del cargo')
    salario = models.DecimalField(max_digits=9,  decimal_places=2, default=0.00, verbose_name='Salario')
    fecha_creado = models.DateTimeField(auto_now=True)
    fecha_actualizado = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.nombre_cargo)

    class Meta:
        verbose_name = 'Cargo'
        verbose_name_plural = 'Cargos'
        ordering = ['id']


class Empleado(models.Model):
    cedula = models.CharField(max_length=20, unique=True, verbose_name='Cedula del empleado')
    nombre = models.CharField(max_length=150, verbose_name='Nombre del empleado')
    apellidos = models.CharField(max_length=150, verbose_name='Apellidos del empleado')
    cargo_id = models.ForeignKey(Cargo, on_delete=models.CASCADE, verbose_name='Cargo')
    estado = models.BooleanField(default=False, verbose_name='Estado')
    fecha_creado = models.DateTimeField(auto_now=True)
    fecha_actualizado = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.nombre)

    class Meta:
        verbose_name = 'Empleado'
        verbose_name_plural = 'Empleados'
        ordering = ['id']


class HoraExtra(models.Model):
    empleado_id = models.ForeignKey(Empleado, on_delete=models.CASCADE, verbose_name='Empleado')
    fecha  = models.DateTimeField(default=datetime.now, verbose_name='Fecha')
    horas_extras = models.PositiveIntegerField(default=0, verbose_name='Horas extras trabajadas')
    fecha_creado = models.DateTimeField(auto_now=True)
    fecha_actualizado = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.empleado_id)

    class Meta:
        verbose_name = 'HoraExtra'
        verbose_name_plural = 'Hora extras trabajadas'
        ordering = ['id']


class EntradaSalida(models.Model):
    empleado_id = models.ForeignKey(Empleado, on_delete=models.CASCADE, verbose_name='Empleado')
    fecha = models.DateTimeField(default=datetime.now, verbose_name='Fecha')
    tipo_registro = models.CharField(max_length=50, verbose_name='Tipo registro (Entrada/Salida)')
    fecha_creado = models.DateTimeField(auto_now=True, verbose_name='Fecha creacion')
    fecha_actualizado = models.DateTimeField(auto_now_add=True, verbose_name='Ultima modificacion')

    def __str__(self):
        return str(self.empleado_id)

    class Meta:
        verbose_name = 'EntradaSalida'
        verbose_name_plural = 'Entrada/Salidas del empleado'
        ordering = ['id']

