from django.contrib import admin

# Register your models here.
from core.models import Cargo, Empleado, HoraExtra, EntradaSalida

class EmpleadoAdmin(admin.ModelAdmin):
    list_display = ('cedula', 'nombre', 'apellidos', 'cargo_id', 'fecha_creado')

class CargoAdmin(admin.ModelAdmin):
    list_display = ('nombre_cargo','salario', 'fecha_creado')

class EntradaSalidaAdmin(admin.ModelAdmin):
    list_display = ('empleado_id', 'tipo_registro', 'fecha')

class HoraExtraAdmin(admin.ModelAdmin):
    list_display = ('empleado_id', 'fecha', 'horas_extras')

admin.site.register(Cargo, CargoAdmin)
admin.site.register(Empleado, EmpleadoAdmin)
admin.site.register(HoraExtra, HoraExtraAdmin)
admin.site.register(EntradaSalida, EntradaSalidaAdmin)